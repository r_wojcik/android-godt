package com.rwojcik.godt.di

import androidx.room.Room
import com.rwojcik.godt.repository.local.GdotDatabase
import com.rwojcik.godt.repository.local.LocalRepository
import org.koin.dsl.module.module

val databaseModule = module {

    single {
        Room.databaseBuilder(get(), GdotDatabase::class.java, "scrabble_database")
            .allowMainThreadQueries()
            .build()
    }

    single { get<GdotDatabase>().recipeDao() }

    single { get<GdotDatabase>().ingredientDao() }

    single { LocalRepository(get(), get(), get()) }

}