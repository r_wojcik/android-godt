package com.rwojcik.godt.di

import com.rwojcik.godt.BuildConfig
import com.rwojcik.godt.repository.network.GdotNetworkRepository
import com.rwojcik.godt.repository.network.GdotService
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single { createOkHttpInterceptor() }

    single {
        Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(get())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    single<GdotService> {
        get<Retrofit>().create(GdotService::class.java)
    }

    single { GdotNetworkRepository(get()) }

}


private fun createOkHttpInterceptor(): OkHttpClient {
    val builder = OkHttpClient.Builder()
    builder.readTimeout(5, TimeUnit.SECONDS)
    builder.writeTimeout(5, TimeUnit.SECONDS)
    builder.callTimeout(5, TimeUnit.SECONDS)
    if(BuildConfig.DEBUG) {
        builder.addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
    }

    return builder.build()
}
