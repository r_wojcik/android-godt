package com.rwojcik.godt.di

import com.rwojcik.godt.ui.list.RecipeListViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelModule = module {

    viewModel { RecipeListViewModel(get(), get()) }

}