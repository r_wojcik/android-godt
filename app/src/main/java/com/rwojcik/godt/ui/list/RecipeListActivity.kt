package com.rwojcik.godt.ui.list

import android.os.Bundle
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.rwojcik.godt.R
import com.rwojcik.godt.repository.local.model.LocalRecipeWithIngredients
import com.rwojcik.godt.util.SimpleTextWatcher
import com.rwojcik.godt.util.bindView
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class RecipeListActivity : AppCompatActivity() {

    private val listViewModel by viewModel<RecipeListViewModel>()

    private val recipeRecyclerView: RecyclerView by bindView(R.id.recipes_recycler_view)
    private val searchEditText: EditText by bindView(R.id.search_edit_text)
    private val swipeRefreshLayout: SwipeRefreshLayout by bindView(R.id.swiper_refresh_layout)

    private val recipesAdapter = RecipesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_list)
        initViews()

        listViewModel.getViewState().observe(this, Observer<RecipeListViewState> {
            Timber.d("Rendering viewState: $it")
            render(it)
        })
    }

    private fun initViews() {
        searchEditText.addTextChangedListener(object : SimpleTextWatcher() {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                listViewModel.searchRecipe(s.toString())
            }
        })

        swipeRefreshLayout.setOnRefreshListener {
            listViewModel.getRecipes()
            searchEditText.text.clear()
        }

        recipeRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recipeRecyclerView.adapter = recipesAdapter
    }


    private fun render(viewState: RecipeListViewState) {
        when(viewState) {
            is RecipeListViewState.Loading -> setProgress(true)
            is RecipeListViewState.Content -> showContent(viewState.recipes)
            is RecipeListViewState.Error -> showError(viewState.message)
        }
    }

    private fun showContent(recipes: List<LocalRecipeWithIngredients>) {
        setProgress(false)
        recipesAdapter.submitList(recipes)
    }

    private fun setProgress(visible: Boolean) {
        swipeRefreshLayout.isRefreshing = visible
        recipeRecyclerView.isVisible = !visible
    }

    private fun showError(@StringRes message: Int) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show()
    }
}
