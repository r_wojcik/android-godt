package com.rwojcik.godt.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.rwojcik.godt.R
import com.rwojcik.godt.repository.local.model.LocalRecipeWithIngredients
import com.rwojcik.godt.util.GlideApp
import com.rwojcik.godt.util.bindView

class RecipesAdapter : ListAdapter<LocalRecipeWithIngredients, RecipesAdapter.RecipesViewHolder>(RECIPES_DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipesViewHolder
        = RecipesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recipe_row, parent, false))


    override fun onBindViewHolder(holder: RecipesViewHolder, position: Int) {
        val item  = getItem(position)
        holder.title.text = item.recipe.title
        holder.ingredients.text = item.ingredients.joinToString(", ") { it.name }
        holder.description.text = item.recipe.description
        GlideApp.with(holder.itemView)
            .load(item.recipe.imageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .into(holder.image)

    }

    class RecipesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val title: TextView by bindView(R.id.title)
        val image: ImageView by bindView(R.id.image)
        val ingredients: TextView by bindView(R.id.ingredients)
        val description: TextView by bindView(R.id.description)
    }

    companion object {
        private val RECIPES_DIFF_CALLBACK = object : DiffUtil.ItemCallback<LocalRecipeWithIngredients>() {
            override fun areItemsTheSame(oldItem: LocalRecipeWithIngredients, newItem: LocalRecipeWithIngredients) =
                oldItem.recipe.id == newItem.recipe.id

            override fun areContentsTheSame(oldItem: LocalRecipeWithIngredients, newItem: LocalRecipeWithIngredients) = oldItem == newItem
        }
    }
}