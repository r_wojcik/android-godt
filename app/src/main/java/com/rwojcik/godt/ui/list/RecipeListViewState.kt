package com.rwojcik.godt.ui.list

import androidx.annotation.StringRes
import com.rwojcik.godt.repository.local.model.LocalRecipeWithIngredients

sealed class RecipeListViewState {

    object Loading : RecipeListViewState()

    data class Content(val recipes: List<LocalRecipeWithIngredients>) : RecipeListViewState()

    data class Error(@StringRes val message: Int) : RecipeListViewState()
}