package com.rwojcik.godt.ui.list

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rwojcik.godt.R
import com.rwojcik.godt.repository.local.LocalRepository
import com.rwojcik.godt.repository.network.GdotNetworkRepository
import com.rwojcik.godt.repository.network.exception.NetworkException
import com.rwojcik.godt.util.plusAssign
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class RecipeListViewModel(
    private val networkRepository: GdotNetworkRepository,
    private val localRepository: LocalRepository
) : ViewModel() {

    private val disposables = CompositeDisposable()
    private var searchDisposable: Disposable? = null

    private val _recipeListViewState = MutableLiveData<RecipeListViewState>(RecipeListViewState.Loading)

    init {
        getRecipes()
    }

    fun getViewState(): LiveData<RecipeListViewState> = _recipeListViewState

    fun getRecipes() {
        disposables += networkRepository.fetchRecipes()
                .flatMap { Single.just(localRepository.insertRecipes(it)) }
                .flatMap { localRepository.getRecipes() }
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext {
                    setErrorViewState(it)
                    localRepository.getRecipes()
                }
                .subscribe(
                    { recipes ->
                        _recipeListViewState.value = RecipeListViewState.Content(recipes)
                    }, {
                        setErrorViewState(it)
                    }
                )

    }

    fun searchRecipe(query: String) {
        searchDisposable?.dispose()
        searchDisposable = localRepository.searchRecipes(query)
               .subscribe({
                    _recipeListViewState.value = RecipeListViewState.Content(it)
               }, {
                    _recipeListViewState.value = RecipeListViewState.Error(R.string.error_search)
               })
    }

    private fun setErrorViewState(throwable: Throwable) {
        when (throwable) {
            is NetworkException -> _recipeListViewState.value =
                    RecipeListViewState.Error(R.string.error_network)
            else -> _recipeListViewState.value = RecipeListViewState.Error(R.string.error_unknown)
        }
    }

    override fun onCleared() {
        disposables.dispose()
        searchDisposable?.dispose()
        super.onCleared()
    }
}