package com.rwojcik.godt

import android.app.Application
import com.facebook.stetho.Stetho
import com.rwojcik.godt.di.appModule
import com.rwojcik.godt.di.databaseModule
import com.rwojcik.godt.di.networkModule
import com.rwojcik.godt.di.viewModelModule
import org.koin.android.ext.android.startKoin
import org.koin.core.Koin
import timber.log.Timber

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        } else {
            //todo add crashlytics
        }

        startKoin(this, listOf(appModule, networkModule, databaseModule, viewModelModule))
    }

}