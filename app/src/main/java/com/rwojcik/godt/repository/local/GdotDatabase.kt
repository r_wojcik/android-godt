package com.rwojcik.godt.repository.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.rwojcik.godt.repository.local.model.LocalIngredient
import com.rwojcik.godt.repository.local.model.LocalRecipe


@Database(
    entities = [
        LocalRecipe::class,
        LocalIngredient::class
    ],
    version = 1
)
abstract class GdotDatabase : RoomDatabase() {

    abstract fun recipeDao(): RecipeDao

    abstract fun ingredientDao(): IngredientDao
}