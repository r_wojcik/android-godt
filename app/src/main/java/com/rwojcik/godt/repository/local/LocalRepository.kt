package com.rwojcik.godt.repository.local

import com.rwojcik.godt.repository.local.model.LocalIngredient
import com.rwojcik.godt.repository.local.model.LocalRecipe
import com.rwojcik.godt.repository.local.model.LocalRecipeWithIngredients
import com.rwojcik.godt.repository.network.model.NetworkRecipe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LocalRepository(
    private val database: GdotDatabase,
    private val recipeDao: RecipeDao,
    private val ingredientDao: IngredientDao
) {

    fun insertRecipes(recipes: List<NetworkRecipe>) {
        database.runInTransaction {
            recipeDao.clear()
            recipes.forEach { recipe ->
                val localRecipe =
                    LocalRecipe(0, recipe.title, recipe.description, recipe.images.firstOrNull()?.url.orEmpty())
                val recipeId = recipeDao.insert(localRecipe)


                val ingredientsNames: List<String> = recipe.ingredients
                    .filter { it.name.isNotBlank() }
                    .map { it.name }
                    .plus(recipe.ingredients.flatMap { it.elements.map { el-> el.name } })

                ingredientsNames
                    .map { LocalIngredient(0, it, recipeId) }
                    .forEach { ingredientDao.insert(it) }
            }
        }
    }

    fun getRecipes(): Single<List<LocalRecipeWithIngredients>> = recipeDao.listRecipesWithIngredients()

    fun searchRecipes(query: String): Single<List<LocalRecipeWithIngredients>> = recipeDao.listRecipesWithIngredients()
        .flatMapObservable { Observable.fromIterable(it) }
        .filter { it.recipe.title.contains(query, true) || it.ingredients.any { ing -> ing.name.contains(query, true) } }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .toList()


}