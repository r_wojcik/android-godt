package com.rwojcik.godt.repository

import com.rwojcik.godt.repository.local.IngredientDao
import com.rwojcik.godt.repository.local.LocalRepository
import com.rwojcik.godt.repository.local.RecipeDao
import com.rwojcik.godt.repository.network.GdotNetworkRepository

class GdotRepository(
    private val networkRepository: GdotNetworkRepository,
    private val localRepository: LocalRepository
) {
}