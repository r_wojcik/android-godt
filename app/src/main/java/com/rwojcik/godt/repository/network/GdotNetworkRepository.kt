package com.rwojcik.godt.repository.network

import com.rwojcik.godt.repository.network.exception.NetworkException
import com.rwojcik.godt.repository.network.exception.UnknownErrorException
import com.rwojcik.godt.repository.network.model.NetworkRecipe
import io.reactivex.Single
import io.reactivex.SingleTransformer
import retrofit2.Response
import java.io.IOException

class GdotNetworkRepository(
    private val service: GdotService
) {

    fun fetchRecipes() : Single<List<NetworkRecipe>> =
        service.getRecipesListDetailed()
            .compose(handleNetworkExceptions())
            .compose(unpackResponseAndHandleErrors())


    private fun <T> unpackResponseAndHandleErrors(): SingleTransformer<Response<T>, T> {
        return SingleTransformer { upstream ->
            upstream.flatMap {
                if (it.isSuccessful) {
                    Single.just(it.body())
                } else {
                    Single.error(UnknownErrorException())
                }
            }
        }
    }

    private fun <T> handleNetworkExceptions(): SingleTransformer<Response<T>, Response<T>> {
        return SingleTransformer { upstream ->
            upstream.onErrorResumeNext { throwable ->
                Single.error(when (throwable) {
                    is IOException -> NetworkException()
                    else -> throwable
                })
            }
        }
    }


}