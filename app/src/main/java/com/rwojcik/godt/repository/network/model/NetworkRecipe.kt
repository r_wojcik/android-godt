package com.rwojcik.godt.repository.network.model

data class NetworkRecipe(
    val title: String,
    val description: String,
    val images: List<NetworkRecipeImage>,
    val ingredients: List<NetworkRecipeIngredient>
)