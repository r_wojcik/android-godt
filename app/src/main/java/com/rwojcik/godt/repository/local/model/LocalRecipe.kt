package com.rwojcik.godt.repository.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @Entity(tableName = "games")
data class Game(
@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int,
@ColumnInfo(name = "name") val name: String,
@ColumnInfo(name = "date") val date: String
//    val players: List<Player> todo add players to game via room?
)
 */

@Entity(tableName = "recipes")
data class LocalRecipe(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "description") val description: String,
    @ColumnInfo(name = "image_url") val imageUrl: String
)