package com.rwojcik.godt.repository.network.model

data class NetworkRecipeImage(
    val url: String
)