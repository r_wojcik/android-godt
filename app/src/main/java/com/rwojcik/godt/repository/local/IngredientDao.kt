package com.rwojcik.godt.repository.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.rwojcik.godt.repository.local.model.LocalIngredient

@Dao
interface IngredientDao {

    @Insert
    fun insert(ingredients: LocalIngredient)

    @Insert
    fun insert(ingredients: List<LocalIngredient>)

    @Query("DELETE FROM ingredients")
    fun clear()

}