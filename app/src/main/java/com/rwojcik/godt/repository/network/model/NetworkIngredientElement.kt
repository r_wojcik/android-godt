package com.rwojcik.godt.repository.network.model

data class NetworkIngredientElement(
    val name: String
)