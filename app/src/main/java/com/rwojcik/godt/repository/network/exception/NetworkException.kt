package com.rwojcik.godt.repository.network.exception

import java.lang.Exception

class NetworkException : Exception()