package com.rwojcik.godt.repository.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "ingredients")
@ForeignKey(
    entity = LocalRecipe::class,
    parentColumns = ["id"],
    childColumns = ["recipe_id"],
    onDelete = ForeignKey.CASCADE
)
data class LocalIngredient(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name ="recipe_id") val recipeId: Long
)