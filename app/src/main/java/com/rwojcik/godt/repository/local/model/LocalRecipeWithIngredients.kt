package com.rwojcik.godt.repository.local.model

import androidx.room.Embedded
import androidx.room.Relation

data class LocalRecipeWithIngredients(

    @Embedded
    val recipe: LocalRecipe,

    @Relation(parentColumn = "id", entityColumn = "recipe_id")
    val ingredients: List<LocalIngredient>

)