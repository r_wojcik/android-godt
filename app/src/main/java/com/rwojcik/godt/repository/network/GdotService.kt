package com.rwojcik.godt.repository.network

import com.rwojcik.godt.repository.network.model.NetworkRecipe
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GdotService {

    @GET("getRecipesListDetailed")
    fun getRecipesListDetailed(
        @Query("size") size: String="thumbnail-medium",
        @Query("ratio") ratio: Int = 1,
        @Query("limit") limit: Int =50,
        @Query("from") from: Int = 0
    ) : Single<Response<List<NetworkRecipe>>>

}