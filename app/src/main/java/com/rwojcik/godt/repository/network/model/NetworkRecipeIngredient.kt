package com.rwojcik.godt.repository.network.model

data class NetworkRecipeIngredient(
    val name: String,
    val elements: List<NetworkIngredientElement>
)