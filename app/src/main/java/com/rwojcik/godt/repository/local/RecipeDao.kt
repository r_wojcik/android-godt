package com.rwojcik.godt.repository.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.rwojcik.godt.repository.local.model.LocalRecipe
import com.rwojcik.godt.repository.local.model.LocalRecipeWithIngredients
import io.reactivex.Single

@Dao
interface RecipeDao {

    @Insert
    fun insert(recipe: LocalRecipe): Long

    @Insert
    fun insert(recipe: List<LocalRecipe>)

    @Transaction @Query("SELECT * FROM recipes")
    fun listRecipesWithIngredients(): Single<List<LocalRecipeWithIngredients>>

    @Query("DELETE FROM recipes")
    fun clear()

}